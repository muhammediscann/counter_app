Firebase Cloud Messaging plugin'ini kullanırken android tarafında eklenmesi gereken class'lar

package <YOUR-PACKAGE-NAME>

import io.flutter.app.FlutterApplication
import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugin.common.PluginRegistry.PluginRegistrantCallback
import io.flutter.plugins.firebasemessaging.FlutterFirebaseMessagingService


public class Application: FlutterApplication(), PluginRegistrantCallback {

    override fun onCreate() {
        super.onCreate()
        FlutterFirebaseMessagingService.setPluginRegistrant(this)
    }

    override fun registerWith(registry: PluginRegistry) {
        FirebaseCloudMessagingPluginRegistrant.registerWith(registry)
    }
}



package <YOUR-PACKAGE-NAME>

import io.flutter.plugin.common.PluginRegistry
import io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin
import com.dexterous.flutterlocalnotifications.FlutterLocalNotificationsPlugin

 class FirebaseCloudMessagingPluginRegistrant {

    companion object {
        fun registerWith(registry: PluginRegistry) {
            if (alreadyRegisteredWith(registry)) {
                return;
            }
            FirebaseMessagingPlugin.registerWith(registry.registrarFor("io.flutter.plugins.firebasemessaging.FirebaseMessagingPlugin"))
			
			 /*
			 * Burası flutter_local_notifications bağımlığını eklediğimiz için var.
			 * Burayı yazmadığımız zaman uygulama kapalı veya alta alınmış halde olduğunda notification gelmemektedir.
			 */
            FlutterLocalNotificationsPlugin.registerWith(registry.registrarFor("com.dexterous.flutterlocalnotifications.FlutterLocalNotificationsPlugin"));
        }

        fun alreadyRegisteredWith(registry: PluginRegistry): Boolean {
            val key = FirebaseCloudMessagingPluginRegistrant::class.java.name
            if (registry.hasPlugin(key)) {
                return true
            }
            registry.registrarFor(key)
            return false
        }
    }
}

-- Bu class'ları ekledikten sonra AndroidManifest dosyasında <application android:name=".Application"...> yapmak gerekiyor.
-- Onun dışında firebase_messaging bağımlılığında geçen maddeler sırasıyla uygulandığında bir problem olmuyor.