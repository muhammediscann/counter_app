import 'package:counter_app/model/ShoppingListItem.dart';
import 'package:counter_app/view_model/ShoppingListItemViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomListItem extends StatefulWidget {
  Map<String, List<ShoppingListItem>> shoppingListItemList;
  int index;

  CustomListItem({this.shoppingListItemList, this.index});

  @override
  _CustomListItemState createState() =>
      _CustomListItemState(shoppingListItemList, index);
}

class _CustomListItemState extends State<CustomListItem> {
  Map<String, List<ShoppingListItem>> _shoppingListItemList;
  int _index;
  bool _isLongPress;

  _CustomListItemState(this._shoppingListItemList, this._index);

  @override
  Widget build(BuildContext context) {
    final ShoppingListItemViewModel _shoppingListItemViewModel =
        Provider.of<ShoppingListItemViewModel>(context);
    var mapKeys = _shoppingListItemList.keys.toList();

    // Silme işleminden sonra listede önceden seçilmiş item'lar koyu renkte olmaya devam ediiyordu.
    if(_shoppingListItemViewModel.willDeletedItem.isEmpty) {
      setState(() {
        _isLongPress = false;
      });
    }
    return GestureDetector(
      child: Container(
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 3.0),
        decoration: _isLongPress
            ? BoxDecoration(
                color: Color.fromRGBO(180, 180, 180, 1),
                borderRadius: BorderRadius.circular(10.0),
              )
            : BoxDecoration(
                color: Theme.of(context).cardColor,
                borderRadius: BorderRadius.circular(10.0),
              ),
        child: ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          leading: Container(
            padding: EdgeInsets.only(right: 10.0),
            decoration: new BoxDecoration(
              border: new Border(
                right: new BorderSide(width: 1.0, color: Colors.red),
              ),
            ),
            child: Icon(
              Icons.apps,
              color: Colors.black,
              size: 30.0,
            ),
          ),
          title: Text(mapKeys[_index],
              style: Theme.of(context).textTheme.title),
          trailing: Text(
            '${_shoppingListItemList[mapKeys[_index]].length}',
            style: Theme.of(context).textTheme.overline,
          ),
          onLongPress: () {
            setState(() {
              _isLongPress = true;
            });
            _addToWillDeletedList(_shoppingListItemViewModel);
          },
          onTap: () {
            setState(() {
              if (_shoppingListItemViewModel.willDeletedItem.isNotEmpty) {
                _isLongPress
                    ? _isLongPress = false
                    : _isLongPress = true;
              }
            });
            _isLongPress
                ? _addToWillDeletedList(_shoppingListItemViewModel)
                : _removeToWillDeletedList(_shoppingListItemViewModel);
          },
        ),
      ),
    );
  }

  // Silinecek olan listeden eleman çıkarma
  void _removeToWillDeletedList(
          ShoppingListItemViewModel _shoppingListItemViewModel) {
    return _shoppingListItemViewModel
          .willDeletedItemRemove(_shoppingListItemList[_shoppingListItemList.keys.toList()[_index]]);
  }

  // Silinecek olan listeye eleman ekleme
  void _addToWillDeletedList(
          ShoppingListItemViewModel _shoppingListItemViewModel) {
    return _shoppingListItemViewModel
          .willDeletedItemAdd(_shoppingListItemList[_shoppingListItemList.keys.toList()[_index]]);
  }
}
