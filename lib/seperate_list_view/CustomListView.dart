import 'package:counter_app/model/ShoppingListItem.dart';
import 'package:flutter/material.dart';

import 'CustomListItem.dart';

class CustomListView extends StatefulWidget {
  Map<String, List<ShoppingListItem>> _shoppingListItemList;
  CustomListView(this._shoppingListItemList);

  @override
  _CustomListViewState createState() => _CustomListViewState(_shoppingListItemList);
}

class _CustomListViewState extends State<CustomListView> {
   Map<String, List<ShoppingListItem>> _shoppingListItemList;
  _CustomListViewState(this._shoppingListItemList);

  @override
  Widget build(BuildContext context) {
    return ListView.separated(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return CustomListItem(
            index: index,
            shoppingListItemList: _shoppingListItemList
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: _shoppingListItemList.length,
    );
  }
}

