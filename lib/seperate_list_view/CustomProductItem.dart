import 'package:counter_app/model/Product.dart';
import 'package:counter_app/view_model/ProductViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CustomProductItem extends StatefulWidget {
  List<Product> productList;
  int index;

  CustomProductItem({this.productList, this.index});

  @override
  _CustomProductItemState createState() =>
      _CustomProductItemState(productList, index);
}

class _CustomProductItemState extends State<CustomProductItem> {
  List<Product> _productList;
  int _index;

  _CustomProductItemState(this._productList, this._index);

  @override
  Widget build(BuildContext context) {
    final ProductViewModel _productViewModel = Provider.of<ProductViewModel>(context);

    // Ekleme işlemi ya da vazgeçme işlemi yapıldıktan sonra Item Create sayfasını 0'lama işlemi
    if(_productViewModel.willAddProductList.isEmpty) {
      setState(() {
        _productList[_index].isCheck = false;
        _productList[_index].number = 0;
      });
    }
    return GestureDetector(
      child: Container(
        margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 3.0),
        decoration: BoxDecoration(
          color: Theme.of(context).cardColor,
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: ListTile(
          contentPadding:
              EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
          leading: CircleAvatar(
            backgroundColor: _productList[_index].isCheck
                ? Theme.of(context).focusColor
                : Theme.of(context).accentColor,
            child: Icon(
              Icons.add,
              color: Theme.of(context).iconTheme.color,
              size: Theme.of(context).iconTheme.size,
            ),
          ),
          title: Text(_findProductName(_productList, _index),
              style: Theme.of(context).textTheme.title),
          trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Visibility(
                visible: _productList[_index].number == 1,
                child: IconButton(
                  icon: Icon(
                    Icons.close,
                    color: Colors.red,
                    size: Theme.of(context).iconTheme.size,
                  ),
                  onPressed: () {
                    setState(() {
                      _productList[_index].number--;
                      _productList[_index].isCheck
                          ? _productList[_index].isCheck = false
                          : _productList[_index].isCheck = true;
                    });
                    _productViewModel.removeToWillAddProductList(_productList[_index]);
                  },
                ),
              ),
              Visibility(
                visible: _productList[_index].number > 1,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                      '${_productList[_index].number}',
                      style: Theme.of(context).textTheme.overline,
                    ),
                    IconButton(
                      icon: Icon(
                        Icons.remove,
                        color: Colors.red,
                        size: Theme.of(context).iconTheme.size,
                      ),
                      onPressed: () {
                        setState(() {
                          if (_productList[_index].number > 0) {
                            _productList[_index].number--;
                          }
                        });
                        _productViewModel.removeToWillAddProductList(_productList[_index]);
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          onTap: () {
            setState(() {
              _productList[_index].number++;
              if (_productList[_index].number < 2) {
                _productList[_index].isCheck
                    ? _productList[_index].isCheck = false
                    : _productList[_index].isCheck = true;
              }
            });
            _productViewModel.addToWillAddProductList(_productList[_index]);
          },
        ),
      ),
    );
  }

  String _findProductName(List<Product> productList, int index) =>
      productList[index].name;
}
