import 'dart:io';

import 'package:flutter/material.dart';

class CustomAlertDiolog extends StatelessWidget {
  String title;
  String description;
  String mainButtonText;
  String generalButtonText;

  CustomAlertDiolog(
      {@required this.title,
      @required this.description,
      @required this.mainButtonText,
      this.generalButtonText});

  Future<bool> show(BuildContext context) async{
    return await showDialog<bool>(context: context, builder: (context) => this, barrierDismissible: false);
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AlertDialog(
        title: Text(
          '$title',
          style: Theme.of(context).textTheme.title,
        ),
        content: Text(
          '$description',
          style: Theme.of(context).textTheme.body1,
        ),
        elevation: 10.0,
        backgroundColor: Theme.of(context).cardColor,
        actions: _buttonSettings(context)
      ),
    );
  }

  List<Widget> _buttonSettings(BuildContext context){
    var buttonList = <Widget>[];
    buttonList.add(
      FlatButton(
        onPressed: () {
          Navigator.of(context).pop(true);
        },
        child: Text(
          mainButtonText,
          style: Theme.of(context).textTheme.button,
        ),
      ),
    );
    if(generalButtonText != null) {
      buttonList.add(
        FlatButton(
          onPressed: () {
            Navigator.of(context).pop(false);
          },
          child: Text(
            generalButtonText,
            style: Theme.of(context).textTheme.button,
          ),
        ),
      );
    }

    return buttonList;
  }
}
