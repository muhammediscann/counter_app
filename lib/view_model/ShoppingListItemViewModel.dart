import 'package:counter_app/data/repository/ShoppingListItemRepository.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:counter_app/model/Product.dart';
import 'package:counter_app/model/ShoppingListItem.dart';
import 'package:flutter/material.dart';

class ShoppingListItemViewModel with ChangeNotifier {
  ShoppingListItemRepository _shoppingListItemRepository =
      getIt<ShoppingListItemRepository>();

  List<ShoppingListItem> _shoppingListItemList = List();
  List<ShoppingListItem> _willDeletedItem = List();
  Map<String, List<ShoppingListItem>> _shoppingListItemMap = Map();


  Map<String, List<ShoppingListItem>> get shoppingListItemMap =>
      _shoppingListItemMap;

  set shoppingListItemMap(Map<String, List<ShoppingListItem>> value) {
    _shoppingListItemMap = value;
  }

  List<ShoppingListItem> get willDeletedItem => _willDeletedItem;

  set willDeletedItem(List<ShoppingListItem> value) {
    _willDeletedItem = value;
  }

  List<ShoppingListItem> get shoppingListItemList => _shoppingListItemList;

  set shoppingListItemList(List<ShoppingListItem> value) {
    _shoppingListItemList = value;
    notifyListeners();
  }

  Future<void> inquireItemList(int shoppingListId) async {
    shoppingListItemList.clear();
    shoppingListItemList = await _shoppingListItemRepository
        .inquireShoppingListItemList(shoppingListId);
    updateShoppingListItemMap();
  }

  /*
  * Alışveriş Listelerinin tutulduğu sayfadan ilgili alışveriş listesinin item'larını
  * bir Map aracaılığı ile sayı kontrolü yapabilmek için build edilen metod.
  * Aynı zamanda item eklendikçe ve item'ler silindikçe de güncel tutulur.
  * */
  void updateShoppingListItemMap () {
    shoppingListItemMap.clear();
    List<ShoppingListItem> list;
    for (ShoppingListItem item in shoppingListItemList) {
      String key = item.product.name;
      if (!shoppingListItemMap.containsKey(key)) {
        list = List();
        list.add(item);
        shoppingListItemMap[key] = list;
      } else {
        list = shoppingListItemMap[key];
        list.add(item);
        shoppingListItemMap[key] = list;
      }
    }
    notifyListeners();
  }

  Future<void> addItem(List<Product> willAddProductList, int shoppingListId) async {
      List<ShoppingListItem> list = await _shoppingListItemRepository.addShoppingListItem(willAddProductList, shoppingListId);
      shoppingListItemList.addAll(list);
      updateShoppingListItemMap();
  }

  Future<OperationResult> deleteShoppingListItemList(
      List<ShoppingListItem> willDeletedShoppingList) async {
    List<int> willDeletedShoppingIdList = willDeletedShoppingList.map((f) => f.shoppingListItemId).toList();
    OperationResult operationResult = await _shoppingListItemRepository.deleteShoppingListItemList(willDeletedShoppingIdList);
    willDeletedShoppingList.forEach((deletedItem){
      shoppingListItemList.remove(deletedItem); // Ana listeden çıkar.
    });
    willDeletedItem.clear(); // Silinecek item listesini temizle
    updateShoppingListItemMap();
    notifyListeners();

    return operationResult;
  }

  void willDeletedItemAdd(List<ShoppingListItem> shoppingListItem){
    willDeletedItem.addAll(shoppingListItem);
    notifyListeners();
  }

  void willDeletedItemRemove(List<ShoppingListItem> shoppingListItem){
    shoppingListItem.forEach((f) => willDeletedItem.remove(f));
    notifyListeners();
  }
}
