import 'package:counter_app/data/repository/ApplicationUserRepository.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/ApplicationUser.dart';
import 'package:flutter/material.dart';

class SearchPeopleViewModel with ChangeNotifier {
  ApplicationUserRepository _applicationUserRepository = getIt<ApplicationUserRepository>();
  ApplicationUser _applicationUser;
  ApplicationUser _myProfileApplicationUser;

  ApplicationUser get applicationUser => _applicationUser;

  set applicationUser(ApplicationUser value) {
    _applicationUser = value;
    notifyListeners();
  }

  ApplicationUser get myProfileApplicationUser => _myProfileApplicationUser;

  set myProfileApplicationUser(ApplicationUser value) {
    _myProfileApplicationUser = value;
    notifyListeners();
  }

  Future<bool> searchApplicationUser(String username) async{
    applicationUser = await _applicationUserRepository
        .searchApplicationUser(username);
    return applicationUser == null ? false:true;
  }

  Future<bool> searchMyProfileApplicationUser(String username) async{
    myProfileApplicationUser = await _applicationUserRepository
        .searchApplicationUser(username);
    return myProfileApplicationUser == null ? false:true;
  }
}