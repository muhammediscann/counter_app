import 'package:counter_app/data/repository/shopping_list_repository.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/shopping_list.dart';
import 'package:flutter/material.dart';

enum ShoppingListViewModelState { INITIAL, LOADED }

class ShoppingListViewModel with ChangeNotifier {
  ShoppingListRepository _shoppingListRepository =
      getIt<ShoppingListRepository>();

  // Alışveriş listelerini tutar
  List<ShoppingList> _shoppingList = List();

  // Uygualama ilk açıldığında veri tabanındaki alışveriş listelerini kontrol etme işini yapar
  ShoppingListViewModelState _state;

  // Alışveriş listelerinin tutulduğu sayfadan detay sayfasına gidildiğinde ilgili alışveriş listesinin adını tutar
  String _shoppingListName;

  ShoppingListViewModel() {
    _state = ShoppingListViewModelState.INITIAL;
  }

  List<ShoppingList> get shoppingList => _shoppingList;

  set shoppingList(List<ShoppingList> value) {
    _shoppingList = value;
    notifyListeners();
  }

  ShoppingListViewModelState get state => _state;

  set state(ShoppingListViewModelState value) {
    _state = value;
    notifyListeners();
  }

  String get shoppingListName => _shoppingListName;

  set shoppingListName(String value) {
    _shoppingListName = value;
    notifyListeners();
  }

  Future<void> addShoppingListName(String name) async {
    ShoppingList value = await _shoppingListRepository.addShoppingList(name);
    if(value != null) {
      shoppingList.add(value);
      notifyListeners();
    }
  }

  Future<void> deleteShoppingList(int shoppingListId) async {
    ShoppingList value =
        await _shoppingListRepository.deleteShoppingList(shoppingListId);
    shoppingList.remove(value);
    notifyListeners();
  }

  Future<void> allList() async {
    shoppingList = await _shoppingListRepository.inquireAll();
    state = ShoppingListViewModelState.LOADED;
  }
}
