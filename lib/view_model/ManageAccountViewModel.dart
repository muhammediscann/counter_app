import 'package:counter_app/data/repository/ApplicationUserRepository.dart';
import 'package:counter_app/data/repository/FirebaseTokenRepository.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/ApplicationUser.dart';
import 'package:counter_app/model/FirebaseToken.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:counter_app/utility/CommonUtility.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ManageAccountViewModel with ChangeNotifier {
  ApplicationUserRepository _applicationUserRepository =
      getIt<ApplicationUserRepository>();
  FirebaseTokenRepository _firebaseTokenRepository =
      getIt<FirebaseTokenRepository>();

  bool _isLogin = false;
  String _token;
  String _username;

  bool get isLogin => _isLogin;

  set isLogin(bool value) {
    _isLogin = value;
    notifyListeners();
  }

  String get token => _token;

  set token(String value) {
    _token = value;
    notifyListeners();
  }


  String get username => _username;

  set username(String value) {
    _username = value;
    notifyListeners();
  }

  void changeLoginState(bool isChange) => isLogin = !isChange;

  // Daha önce random bir username ile kayıt yapılan kullanıcıyı update et!
  Future<bool> registerApplicationUser(ApplicationUser applicationUser) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    String username = _sharedPreferences.getString("username");
    if (username != null) {
      ApplicationUser registeredAplUser = await _applicationUserRepository
          .inquireApplicationUser(ApplicationUser.withoutPass(username));
      registeredAplUser.username = applicationUser.username;
      registeredAplUser.email = applicationUser.email;
      registeredAplUser.password = applicationUser.password;
      OperationResult registerUserUpdateResult =
          await _applicationUserRepository
              .updateRegisterApplicationUser(registeredAplUser);
      if (CommonUtility.isOperationResultSuccess(registerUserUpdateResult)) {
        await _sharedPreferences.setString(
            "username", registeredAplUser.username);
        return true;
      }
      return false;
    }
  }

  Future<void> loginApplicationUser(ApplicationUser applicationUser) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    String username = _sharedPreferences.getString("username");
    if (username != null) {
      ApplicationUser registeredAplUser = await _applicationUserRepository
          .inquireApplicationUser(ApplicationUser.withoutPass(username));
      registeredAplUser.username = applicationUser.username;
      registeredAplUser.password = applicationUser.password;
      OperationResult registerUserUpdateResult =
          await _applicationUserRepository
              .loginApplicationUser(registeredAplUser);
      if (CommonUtility.isOperationResultSuccess(registerUserUpdateResult)) {
        // Buraya girdiyse jwt token kaydedildi anlamına gelir.
        isExistToken();
        if (isLogin) {
          await _firebaseTokenRepository.createFirebaseToken(FirebaseToken(
              token: _sharedPreferences.getString('firebase_token'),
              applicationUserId: registeredAplUser.applicationUserId));
        }
      }
    }
  }

  Future<bool> logout() async {
    SharedPreferences _sharedPreferences =
    await SharedPreferences.getInstance();
    _sharedPreferences.remove("jwt_token");
  }

    Future<void> isExistToken() async{
      SharedPreferences _sharedPreferences =
          await SharedPreferences.getInstance();
    isLogin = _sharedPreferences.containsKey('jwt_token');
    isLogin ? token = _sharedPreferences.getString("jwt_token") : token = null;
    username = _sharedPreferences.getString("username");
  }
}
