import 'package:counter_app/data/repository/ProductRepository.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/Product.dart';
import 'package:flutter/material.dart';

class ProductViewModel with ChangeNotifier {

  ProductRepository _productRepository = getIt<ProductRepository>();

  // Popular product'lar arasında arama yapmadan seçim yapılmasını sağlar.
  List<Product> _popularProductList = List();

  // Bu liste populer item'ları veya search sırasında aranan item'ları tutar.
  List<Product> _searchProductList = List();

  // Bu liste search sırasında ya da popular listelerin arasından seçilen product'ları tutar.
  List<Product> _willAddProductList = List();

  // Item Crate sayfasındaki navigation bar'ın kaldırılıp kaldırılmayacağını tutar.
  bool _isChanged = false;

  List<Product> get popularProductList => _popularProductList;

  set popularProductList(List<Product> value) {
    _popularProductList = value;
    notifyListeners();
  }


  List<Product> get searchProductList => _searchProductList;

  set searchProductList(List<Product> value) {
    _searchProductList = value;
  }

  bool get isChanged => _isChanged;

  set isChanged(bool value) {
    _isChanged = value;
    notifyListeners();
  }


  List<Product> get willAddProductList => _willAddProductList;

  set willAddProductList(List<Product> value) {
    _willAddProductList = value;
  }

  Future<void> inquirePopularProductList() async{
    isChanged = false; // Search yapıldığında Navigation bar'ı kaldırmak için
    if(popularProductList.isEmpty) {
      popularProductList = await _productRepository.inquirePopularProductList();
    }
  }

  void searchOperation(String searchText) {
    searchProductList.clear(); // Search yapıldığında önceki aranılan listenin temizlenmesi
    isChanged = true;  // Item create sayfasında arama yapıldığında navigation bar'ı kaldırır.

    for(Product product in popularProductList) {
      if(product.name.toLowerCase().contains(searchText.toLowerCase())) {
        searchProductList.add(product);
      }
    }

    if(_searchProductList.isEmpty) {
      searchProductList.add(Product.withoutCategory(searchText));
    }
    notifyListeners();
  }

  void addToWillAddProductList(Product product) {
    willAddProductList.add(product);
    notifyListeners();
  }

  void removeToWillAddProductList(Product product) {
    willAddProductList.remove(product);
    notifyListeners();
  }
}