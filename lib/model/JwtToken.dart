import 'dart:convert';

class JwtToken {
  String jwtToken;

  JwtToken({this.jwtToken});

  static JwtToken operationResultFromJson(String str) => JwtToken.fromJson(json.decode(str));

  String operationResultToJson(JwtToken data) => json.encode(data.toJson());

  factory JwtToken.fromJson(Map<String, dynamic> json) => JwtToken(
    jwtToken: json["jwtToken"],
  );

  Map<String, dynamic> toJson() => {
    "jwtToken": jwtToken,
  };
}