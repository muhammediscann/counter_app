import 'dart:convert';

class OperationResult {
  String operationResultCode;
  String description;

  OperationResult({
    this.operationResultCode,
    this.description,
  });

  static OperationResult operationResultFromJson(String str) => OperationResult.fromJson(json.decode(str));

  String operationResultToJson(OperationResult data) => json.encode(data.toJson());

  factory OperationResult.fromJson(Map<String, dynamic> json) => OperationResult(
    operationResultCode: json["operationResultCode"],
    description: json["description"],
  );

  Map<String, dynamic> toJson() => {
    "operationResultCode": operationResultCode,
    "description": description,
  };
}
