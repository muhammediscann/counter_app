import 'dart:convert';

class ShoppingList {
  int shoppingListId;
  String name;

  ShoppingList({
    this.shoppingListId,
    this.name,
  });

  ShoppingList.withoutName(this.shoppingListId);

  static ShoppingList shoppingListFromJson(String str) =>
      ShoppingList.fromJson(json.decode(str));

  static List<ShoppingList> shoppingListFromJsonList(String str) =>
      List<ShoppingList>.from(
          json.decode(str).map((x) => ShoppingList.fromJson(x)));

  String shoppingListToJson(ShoppingList data) => json.encode(data.toJson());

  factory ShoppingList.fromJson(Map<String, dynamic> json) => ShoppingList(
        shoppingListId: json["shoppingListId"],
        name: json["name"] != null ? json["name"] : null,
      );

  Map<String, dynamic> toJson() => {
        "shoppingListId": shoppingListId != null ? shoppingListId : null,
        "name": name != null ? name : null,
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ShoppingList &&
          runtimeType == other.runtimeType &&
          shoppingListId == other.shoppingListId &&
          name == other.name;

  @override
  int get hashCode => shoppingListId.hashCode ^ name.hashCode;
}
