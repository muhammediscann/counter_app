import 'dart:convert';
import 'Category.dart';

class Product {
  int productId;
  String name;
  Category category;
  bool _isCheck = false;
  int _number = 0;

  Product({
    this.productId,
    this.name,
    this.category,
  });


  bool get isCheck => _isCheck;

  set isCheck(bool value) {
    _isCheck = value;
  }


  int get number => _number;

  set number(int value) {
    _number = value;
  }

  Product.withoutCategory(this.name);

  static List<Product> productFromJson(String str) => List<Product>.from(json.decode(str).map((x) => Product.fromJson(x)));

  String productToJson(List<Product> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    productId: json["productId"] != null ? json["productId"] : null,
    name: json["name"],
    category: json["category"] != null ? Category.fromJson(json["category"]) : null,
  );

  Map<String, dynamic> toJson() => {
    "productId": productId != null ? productId : null,
    "name": name != null ? name : null,
    "category": category != null ? category.toJson() : null,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Product &&
          runtimeType == other.runtimeType &&
          productId == other.productId &&
          name == other.name &&
          category == other.category;

  @override
  int get hashCode => productId.hashCode ^ name.hashCode ^ category.hashCode;
}