class FirebaseToken{
  int firebaseTokenId;
  String token;
  int applicationUserId;

  FirebaseToken({
    this.firebaseTokenId,
    this.token,
    this.applicationUserId,
  });

  factory FirebaseToken.fromJson(Map<String, dynamic> json) => FirebaseToken(
    firebaseTokenId: json["firebaseTokenId"] != null ? json["firebaseTokenId"] : null,
    token: json["token"] != null ? json["token"] : null,
    applicationUserId: json["applicationUserId"] != null ? json["applicationUserId"] : null,
  );

  Map<String, dynamic> toJson() => {
    "firebaseTokenId": firebaseTokenId != null ? firebaseTokenId : null,
    "token": token != null ? token : null,
    "applicationUserId": applicationUserId != null ? applicationUserId : null,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is FirebaseToken &&
          runtimeType == other.runtimeType &&
          firebaseTokenId == other.firebaseTokenId &&
          token == other.token &&
          applicationUserId == other.applicationUserId;

  @override
  int get hashCode =>
      firebaseTokenId.hashCode ^ token.hashCode ^ applicationUserId.hashCode;
}