import 'dart:convert';

class ApplicationUser {
  int applicationUserId;
  String username;
  String email;
  String password;

  ApplicationUser({this.applicationUserId, this.username, this.email, this.password});

  ApplicationUser.withoutPass(this.username);

  static ApplicationUser applicationUserFromJson(String str) => ApplicationUser.fromJson(json.decode(str));

  factory ApplicationUser.fromJson(Map<String, dynamic> json) => ApplicationUser(
    applicationUserId: json["applicationUserId"] != null ? json["applicationUserId"] : null,
    username: json["username"] != null ? json["username"] : null,
    email: json["email"] != null ? json["email"] : null,
    password: json["password"] != null ? json["password"] : null,
  );

  Map<String, dynamic> toJson() => {
    "applicationUserId": applicationUserId != null ? applicationUserId : null,
    "username": username != null ? username : null,
    "email": email != null ? email : null,
    "password": password != null ? password : null,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ApplicationUser &&
          runtimeType == other.runtimeType &&
          applicationUserId == other.applicationUserId &&
          username == other.username &&
          email == other.email &&
          password == other.password;

  @override
  int get hashCode =>
      applicationUserId.hashCode ^ username.hashCode ^ password.hashCode;

  @override
  String toString() {
    return 'ApplicationUser{applicationUserId: $applicationUserId, username: $username, email: $email, password: $password}';
  }
}