class Category {
  int categoryId;
  String name;

  Category({
    this.categoryId,
    this.name,
  });

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    categoryId: json["categoryId"] != null ? json["categoryId"] : null,
    name: json["name"] != null ? json["name"] : null,
  );

  Map<String, dynamic> toJson() => {
    "categoryId": categoryId != null ? categoryId : null,
    "name": name != null ? name : null,
  };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Category &&
          runtimeType == other.runtimeType &&
          categoryId == other.categoryId &&
          name == other.name;

  @override
  int get hashCode => categoryId.hashCode ^ name.hashCode;
}