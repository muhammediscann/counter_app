import 'dart:convert';

import 'Product.dart';
import 'shopping_list.dart';

class ShoppingListItem {
  int shoppingListItemId;
  Product product;
  ShoppingList shoppingList;
  bool _isCheck = false;

  ShoppingListItem({
    this.shoppingListItemId,
    this.product,
    this.shoppingList,
  });

  bool get isCheck => _isCheck;

  set isCheck(bool value) {
    _isCheck = value;
  }

  static List<ShoppingListItem> shoppingListItemFromJsonList(String str) =>
      List<ShoppingListItem>.from(
          json.decode(str).map((x) => ShoppingListItem.fromJson(x)));

  static String shoppingListItemToJson(List<ShoppingListItem> data) =>
      json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

  static ShoppingListItem shoppingListItemFromJson(String str) =>
      ShoppingListItem.fromJson(json.decode(str));

  factory ShoppingListItem.fromJson(Map<String, dynamic> json) =>
      ShoppingListItem(
        shoppingListItemId: json["shoppingListItemId"],
        product: Product.fromJson(json["product"]),
        shoppingList: ShoppingList.fromJson(json["shoppingList"]),
      );

  Map<String, dynamic> toJson() => {
        "shoppingListItemId": shoppingListItemId != null ? shoppingListItemId : null,
        "product": product.toJson(),
        "shoppingList": shoppingList.toJson(),
      };

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is ShoppingListItem &&
          runtimeType == other.runtimeType &&
          shoppingListItemId == other.shoppingListItemId &&
          product == other.product &&
          shoppingList == other.shoppingList;

  @override
  int get hashCode =>
      shoppingListItemId.hashCode ^ product.hashCode ^ shoppingList.hashCode;
}
