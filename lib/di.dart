import 'package:counter_app/data/FirebaseTokenClient.dart';
import 'package:counter_app/data/repository/FirebaseTokenRepository.dart';
import 'package:get_it/get_it.dart';
import 'data/ApplicationUserClient.dart';
import 'data/ProductClient.dart';
import 'data/ShoppingListItemClient.dart';
import 'data/repository/ApplicationUserRepository.dart';
import 'data/repository/ProductRepository.dart';
import 'data/repository/ShoppingListItemRepository.dart';
import 'data/repository/shopping_list_repository.dart';
import 'data/shopping_list_client.dart';

final getIt = GetIt.instance;

void setup() {
  getIt.registerLazySingleton<ShoppingListClient>(() => ShoppingListClient());
  getIt.registerLazySingleton<ShoppingListRepository>(() => ShoppingListRepository());
  getIt.registerLazySingleton<ProductClient>(() => ProductClient());
  getIt.registerLazySingleton<ProductRepository>(() => ProductRepository());
  getIt.registerLazySingleton<ShoppingListItemClient>(() => ShoppingListItemClient());
  getIt.registerLazySingleton<ShoppingListItemRepository>(() => ShoppingListItemRepository());
  getIt.registerLazySingleton<ApplicationUserClient>(() => ApplicationUserClient());
  getIt.registerLazySingleton<ApplicationUserRepository>(() => ApplicationUserRepository());
  getIt.registerLazySingleton<FirebaseTokenClient>(() => FirebaseTokenClient());
  getIt.registerLazySingleton<FirebaseTokenRepository>(() => FirebaseTokenRepository());
}