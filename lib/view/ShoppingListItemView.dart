import 'package:counter_app/model/OperationResult.dart';
import 'package:counter_app/common/CustomAlertDiolog.dart';
import 'package:counter_app/seperate_list_view/CustomListView.dart';
import 'package:counter_app/view_model/ProductViewModel.dart';
import 'package:counter_app/view_model/ShoppingListItemViewModel.dart';
import 'package:counter_app/view_model/shopping_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'create/ShoppingListItemCreateView.dart';

class ShoppingListItemView extends StatefulWidget {
  int shoppingListId;

  ShoppingListItemView([this.shoppingListId]);

  @override
  _ShoppingListItemViewState createState() =>
      _ShoppingListItemViewState(shoppingListId);
}

class _ShoppingListItemViewState extends State<ShoppingListItemView> {
  int shoppingListId;

  _ShoppingListItemViewState([this.shoppingListId]);

  @override
  Widget build(BuildContext context) {
    final ShoppingListViewModel shoppingListViewModel =
    Provider.of<ShoppingListViewModel>(context);
    final ProductViewModel _productViewModel =
    Provider.of<ProductViewModel>(context);
    final ShoppingListItemViewModel _shoppingListItemViewModel =
    Provider.of<ShoppingListItemViewModel>(context);
    var itemMap = _shoppingListItemViewModel.shoppingListItemMap;
    return Scaffold(
      backgroundColor: Theme
          .of(context)
          .backgroundColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text('${shoppingListViewModel.shoppingListName}'),
        actions: <Widget>[
          _buildDeleteButton(_shoppingListItemViewModel, context)
        ],
      ),
      body: itemMap.isNotEmpty
          ? CustomListView(
          itemMap
      )
          : Center(
        child: Text(
          'Ürün eklemek için + ya dokunun!',
          style: Theme
              .of(context)
              .textTheme
              .title,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Add Shppping List Item',
        child: Icon(Icons.add),
        heroTag: "ShoppingListItemCreate",
        onPressed: () {
          _productViewModel.inquirePopularProductList();
          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) =>
                    ShoppingListItemCreateView(shoppingListId)),
          );
        },
      ),
    );
  }

  Visibility _buildDeleteButton(
      ShoppingListItemViewModel _shoppingListItemViewModel,
      BuildContext context) {
    return Visibility(
      visible: _shoppingListItemViewModel.willDeletedItem.isNotEmpty,
      child: IconButton(
        icon: Icon(
          Icons.delete,
          color: Theme
              .of(context)
              .appBarTheme
              .actionsIconTheme
              .color,
        ),
        onPressed: () async {
          OperationResult operationResult =
          await _shoppingListItemViewModel.deleteShoppingListItemList(
              _shoppingListItemViewModel.willDeletedItem);
          CustomAlertDiolog(title: operationResult.operationResultCode,
              description: operationResult.description,
              mainButtonText: "OK").show(context);
        },
      ),
    );
  }
}
