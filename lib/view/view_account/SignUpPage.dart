import 'package:counter_app/model/ApplicationUser.dart';
import 'package:counter_app/view/ShoppingListView.dart';
import 'package:counter_app/view_model/ManageAccountViewModel.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'LoginPage.dart';
import 'widget/BezierContainer.dart';

class SignUpPage extends StatefulWidget {
  SignUpPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  var _formKey = GlobalKey<FormState>();
  ManageAccountViewModel _manageAccountViewModel;
  ApplicationUser _registerApplicationUser = ApplicationUser();

  Widget _backButton() {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Row(
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
              child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            ),
            Text('Back',
                style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
          ],
        ),
      ),
    );
  }

  Widget _submitButton() {
    return RaisedButton(
      onPressed: () => _registerApplicationUserButton(),
      child: Container(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.symmetric(vertical: 15),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(5)),
        ),
        child: Text("Register"),
      ),
    );
  }

  Widget _loginAccountLabel() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      alignment: Alignment.bottomCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            'Already have an account ?',
            style: TextStyle(fontSize: 13, fontWeight: FontWeight.w600),
          ),
          SizedBox(
            width: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginPage()));
            },
            child: Text(
              'Login',
              style: TextStyle(
                  color: Theme.of(context).accentColor,
                  fontSize: 13,
                  fontWeight: FontWeight.w600),
            ),
          )
        ],
      ),
    );
  }

  Widget _title() {
    return RichText(
      textAlign: TextAlign.center,
      text: TextSpan(
          text: 'shopping',
          style: GoogleFonts.portLligatSans(
            textStyle: Theme.of(context).textTheme.display1,
            fontSize: 30,
            fontWeight: FontWeight.w700,
            color: Colors.black,
          ),
          children: [
            TextSpan(
              text: 'smile',
              style: TextStyle(color: Colors.blue, fontSize: 30),
            ),
          ]),
    );
  }

  Widget _emailPasswordWidget() {
    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          _usernameTextField(),
          _emailTextField(),
          _passwordTextField(),
        ],
      ),
    );
  }

  Container _passwordTextField() {
    return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Password',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _formKey.currentState.reset();
                        });
                      },
                      icon: Icon(Icons.clear),
                    ),
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                onSaved: (value) => _registerApplicationUserTextField(value, "Password"),
                validator: (String value) =>
                    value.isEmpty ? "Password boş bırakılamaz" : null,
              )
            ],
          ),
        );
  }

  Container _emailTextField() {
    return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Email',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _formKey.currentState.reset();
                        });
                      },
                      icon: Icon(Icons.clear),
                    ),
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                onSaved: (value) => _registerApplicationUserTextField(value, "Email"),
                validator: (String value) =>
                    value.isEmpty ? "Email boş bırakılamaz" : null,
              )
            ],
          ),
        );
  }

  Container _usernameTextField() {
    return Container(
          margin: EdgeInsets.symmetric(vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                'Username',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 15),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                decoration: InputDecoration(
                    suffixIcon: IconButton(
                      onPressed: () {
                        setState(() {
                          _formKey.currentState.reset();
                        });
                      },
                      icon: Icon(Icons.clear),
                    ),
                    border: InputBorder.none,
                    fillColor: Color(0xfff3f3f4),
                    filled: true),
                onSaved: (value) => _registerApplicationUserTextField(value, "Username"),
                validator: (String value) =>
                    value.isEmpty ? "Username boş bırakılamaz" : null,
              )
            ],
          ),
        );
  }

  @override
  Widget build(BuildContext context) {
    _manageAccountViewModel = Provider.of<ManageAccountViewModel>(context);
    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: <Widget>[
          Container(
            padding: EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  flex: 3,
                  child: SizedBox(),
                ),
                _title(),
                SizedBox(
                  height: 50,
                ),
                _emailPasswordWidget(),
                SizedBox(
                  height: 20,
                ),
                _submitButton(),
                Expanded(
                  flex: 2,
                  child: SizedBox(),
                )
              ],
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: _loginAccountLabel(),
          ),
          Positioned(top: 40, left: 0, child: _backButton()),
          Positioned(
              top: -MediaQuery.of(context).size.height * .15,
              right: -MediaQuery.of(context).size.width * .4,
              child: BezierContainer())
        ],
      ),
    )));
  }

  void _registerApplicationUserTextField(String value, String title) {
    switch (title) {
      case 'Username':
        {
          _registerApplicationUser.username = value;
        }
        break;

      case 'Email':
        {
          _registerApplicationUser.email = value;
        }
        break;

      case 'Password':
        {
          _registerApplicationUser.password = value;
        }
        break;
    }
  }

  void _registerApplicationUserButton() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      bool isRegisterApplcitionUserUpdate = await _manageAccountViewModel
          .registerApplicationUser(_registerApplicationUser);
      if (isRegisterApplcitionUserUpdate) {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => LoginPage()));
      }
    }
  }
}
