import 'package:counter_app/view/ShoppingListView.dart';
import 'package:counter_app/view_model/ManageAccountViewModel.dart';
import 'package:counter_app/view_model/SearchPeopleViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MyProfileView extends StatefulWidget {
  @override
  _MyProfileViewState createState() => _MyProfileViewState();
}

class _MyProfileViewState extends State<MyProfileView> {
  bool _isApplicationlUser = false;

  @override
  Widget build(BuildContext context) {
    ManageAccountViewModel _manageAccountViewModel =
        Provider.of<ManageAccountViewModel>(context);
    SearchPeopleViewModel _searchPeopleViewModel =
        Provider.of<SearchPeopleViewModel>(context);
    _searchPeopleViewModel
        .searchMyProfileApplicationUser(_manageAccountViewModel.username)
        .then((isAplUser) => _isApplicationlUser = isAplUser);
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('My Profile'),
      ),
      body: Container(
        margin: EdgeInsets.all(10.0),
        child: Column(
          children: <Widget>[
            Text(
              "Summary",
              style: Theme.of(context).textTheme.title,
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: <Widget>[
                Text(
                  'Username : ',
                  style: Theme.of(context).textTheme.button,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  _isApplicationlUser
                      ? _searchPeopleViewModel.myProfileApplicationUser.username
                      : "",
                  style: Theme.of(context).textTheme.body1,
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(
              children: <Widget>[
                Text(
                  'Email : ',
                  style: Theme.of(context).textTheme.button,
                ),
                SizedBox(
                  width: 10.0,
                ),
                Text(
                  _isApplicationlUser
                      ? _searchPeopleViewModel.myProfileApplicationUser.email
                      : "",
                  style: Theme.of(context).textTheme.body1,
                ),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            RaisedButton(
              onPressed: () {
                _manageAccountViewModel.logout();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => ShoppingListView()),
                );
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(vertical: 15),
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(5)),
                ),
                child: Text("Logout"),
              ),
            )
          ],
        ),
      ),
    );
  }
}
