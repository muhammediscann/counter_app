import 'package:counter_app/common/CustomAlertDiolog.dart';
import 'package:counter_app/model/ApplicationUser.dart';
import 'package:counter_app/utility/OperationResultCodeEnum.dart';
import 'package:counter_app/view_model/SearchPeopleViewModel.dart';
import 'package:counter_app/view_model/shopping_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';

class SearchPeople extends StatefulWidget {
  @override
  _SearchPeopleState createState() => _SearchPeopleState();
}

class _SearchPeopleState extends State<SearchPeople>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  SearchPeopleViewModel _searchPeopleViewModel;
  ShoppingListViewModel _shoppingListViewModel;
  String _username;
  TabController _tabController;
  ProgressDialog _progressDialog;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    _searchPeopleViewModel = Provider.of<SearchPeopleViewModel>(context);
    _shoppingListViewModel = Provider.of<ShoppingListViewModel>(context);
    _progressDialog = ProgressDialog(context,
        type: ProgressDialogType.Normal,
        isDismissible: false,
        showLogs: true);
    _progressDialog.style(
      message: 'Searching'
    );
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Form(
          key: _formKey,
          child: TextFormField(
            decoration: InputDecoration(
                prefixIcon: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
                hintText: 'Enter username',
                hintStyle: Theme.of(context).textTheme.body2),
            style: TextStyle(color: Colors.white),
            onSaved: (value) {
              _username = value;
            },
            validator: (String value) =>
                value.isEmpty ? "Boş bırakılamaz" : null,
          ),
        ),
        actions: <Widget>[
          FlatButton(
            onPressed: () => _pressOkButton(),
            child: Text(
              'OK',
              style: Theme.of(context).textTheme.button,
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            TabBar(
              tabs: <Widget>[
                Tab(
                  icon: Text(
                    'NEW',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
                Tab(
                  icon: Text(
                    'OLD',
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
              controller: _tabController,
            ),
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: TabBarView(
                  controller: _tabController,
                  children: <Widget>[
                    newPerson(),
                    oldPersonList(),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget newPerson() {
    var visibility = _searchPeopleViewModel.applicationUser != null;
    return Center(
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Visibility(
              visible: visibility,
              child: Column(
                children: <Widget>[
                  GestureDetector(
                    child: Container(
                      margin: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 3.0),
                      decoration: BoxDecoration(
                        color: Theme.of(context).cardColor,
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                      child: ListTile(
                        contentPadding: EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 10.0),
                        leading: Container(
                          padding: EdgeInsets.only(right: 10.0),
                          decoration: new BoxDecoration(
                            border: new Border(
                              right:
                                  new BorderSide(width: 1.0, color: Colors.red),
                            ),
                          ),
                          child: Icon(
                            Icons.apps,
                            color: Colors.black,
                            size: 30.0,
                          ),
                        ),
                        title: Text(visibility ? _searchPeopleViewModel.applicationUser.username : "",
                            style: Theme.of(context).textTheme.title),
                        onTap: () => _pressSearchedPersonButton(),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Container(
                    margin: EdgeInsets.all(10.0),
                    child: Column(
                      children: <Widget>[
                        Text("Summary", style: Theme.of(context).textTheme.title,),
                        SizedBox(height: 20.0,),
                        Row(
                          children: <Widget>[
                            Text('Shopping List Name : ', style: Theme.of(context).textTheme.button,),
                            SizedBox(width: 10.0,),
                            Text(_shoppingListViewModel.shoppingListName, style: Theme.of(context).textTheme.body1,),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Visibility(
              visible: !visibility,
              child: Text('Search the person to share with!'),
            ),
          ],
        ),
      ),
    );
  }

  void _pressOkButton() async{
    if (_formKey.currentState.validate()) {
      await _progressDialog.show();
      _formKey.currentState.save();
      bool isHasApplicationuser = await _searchPeopleViewModel.searchApplicationUser(_username);
      await _progressDialog.hide();
      if(!isHasApplicationuser) {
        CustomAlertDiolog(
          title: OperationResultCodeEnum.INFORMATION.toShortString(),
          description: 'User not found!',
          mainButtonText: 'OK',
        ).show(context);
      }
    }
  }

  Future<void> _pressSearchedPersonButton() async {
    await CustomAlertDiolog(
      title: OperationResultCodeEnum.INFORMATION.toShortString(),
      description: 'Are you sure you want to share?',
      mainButtonText: 'YES',
      generalButtonText: 'NO',
    ).show(context);
  }

  Widget oldPersonList() {
    return Text('GEelecek!!!');
  }
}

// Dart dilindeki enum kullanımı JAVAYA göre bayağı bir berbat durumda
extension on OperationResultCodeEnum {
  String toShortString() {
    return this.toString().split('.').last;
  }
}
