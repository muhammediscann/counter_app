import 'package:counter_app/view_model/shopping_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShoppingListCreateView extends StatefulWidget {
  @override
  _ShoppingListCreateViewState createState() => _ShoppingListCreateViewState();
}

class _ShoppingListCreateViewState extends State<ShoppingListCreateView> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    var _shoppingListViewModel = Provider.of<ShoppingListViewModel>(context);
    String _shoppingListName;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          icon: Hero(
            tag: "ShoppingListCreate",
            child: Icon(
              Icons.clear,
              color: Colors.white,
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(10.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  TextFormField(
                    decoration: InputDecoration(
                      hintText: 'Yeni Liste',
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(10.0),
                      ),
                    ),
                    onSaved: (String value) {
                      _shoppingListName = value;
                    },
                    validator: (String value) => value.isEmpty ? "Boş bırakılamaz" : null,
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  RaisedButton(
                    child: Text('OLUŞTUR'),
                    onPressed: () {
                      if (_formKey.currentState.validate()) {
                        _formKey.currentState.save();
                        _shoppingListViewModel
                            .addShoppingListName(_shoppingListName);
                        Navigator.pop(context);
                      }
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
