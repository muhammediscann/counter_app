import 'package:counter_app/model/Product.dart';
import 'package:counter_app/seperate_list_view/CustomProductItem.dart';
import 'package:counter_app/view_model/ProductViewModel.dart';
import 'package:counter_app/view_model/ShoppingListItemViewModel.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ShoppingListItemCreateView extends StatefulWidget {
  int shoppingListId;

  ShoppingListItemCreateView([this.shoppingListId]);

  @override
  _ShoppingListItemCreateViewState createState() =>
      _ShoppingListItemCreateViewState(shoppingListId);
}

class _ShoppingListItemCreateViewState extends State<ShoppingListItemCreateView>
    with SingleTickerProviderStateMixin {
  final searchController = TextEditingController();
  TabController _tabController;
  ProductViewModel _productViewModel;

  int shoppingListId;
  _ShoppingListItemCreateViewState(this.shoppingListId);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _productViewModel = Provider.of<ProductViewModel>(context);
    ShoppingListItemViewModel _shoppingListItemViewModel =
        Provider.of<ShoppingListItemViewModel>(context);
    var _searchingProductList = _productViewModel.searchProductList;
    var _popularProductList = _productViewModel.popularProductList;
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: TextField(
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.search,
                color: Colors.white,
              ),
            ),
            style: TextStyle(color: Colors.white),
            controller: searchController,
            onChanged: (value) {
              _productViewModel.searchOperation(value);
            },
          ),
          leading: IconButton(
            icon: Hero(
              tag: "ShoppingListItemCreate",
              child: Icon(
                Icons.clear,
                color: Colors.white,
              ),
            ),
            onPressed: () {
              // Eklenecek olarak seçilen ürünlerin vazgeçme işleminden sonra silinmesi
              _productViewModel.willAddProductList.clear();
              Navigator.pop(context);
            },
          ),
          actions: <Widget>[
            Visibility(
              visible: _productViewModel.willAddProductList.isNotEmpty,
              child: FlatButton(
                onPressed: () async{
                  await _shoppingListItemViewModel.addItem(_productViewModel.willAddProductList,
                      shoppingListId);
                  // Eklenecek olarak seçilen ürünlerin ekleme işlemden sonra silinmesi
                  _productViewModel.willAddProductList.clear();
                  Navigator.of(context).pop();
                },
                child: Text('BITTI'),
              ),
            )
          ],
        ),
        body: _productViewModel.isChanged
            ? _popularOrNewProductList(_searchingProductList)
            : safeArea(_popularProductList));
  }

  Widget safeArea(List<Product> popularProductList) {
    return SafeArea(
      child: Column(
        children: <Widget>[
          TabBar(
            tabs: <Widget>[
              Tab(
                icon: Text(
                  'POPULAR',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
              Tab(
                icon: Text(
                  'CATEGORY',
                  style: TextStyle(
                      color: Colors.black, fontWeight: FontWeight.bold),
                ),
              ),
            ],
            controller: _tabController,
          ),
          Expanded(
            child: Container(
              height: MediaQuery.of(context).size.height,
              child: TabBarView(
                controller: _tabController,
                children: <Widget>[
                  _popularOrNewProductList(popularProductList),
                  Icon(Icons.settings), // TODO Bu alana category'e göre product'lar eklenecektir.
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _popularOrNewProductList(List<Product> productList) {
    return ListView.separated(
        reverse: true,
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        itemBuilder: (BuildContext context, int index) {
          return CustomProductItem(
            productList: productList,
            index: index,
          );
        },
        separatorBuilder: (BuildContext context, int index) => const Divider(),
        itemCount: productList.length);
  }
}
