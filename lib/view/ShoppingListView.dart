import 'package:counter_app/common/CustomAlertDiolog.dart';
import 'package:counter_app/model/shopping_list.dart';
import 'package:counter_app/notification/NotificationHandler.dart';
import 'package:counter_app/utility/OperationResultCodeEnum.dart';
import 'package:counter_app/view/create/ShoppingListCreateView.dart';
import 'package:counter_app/view/view_account/MyProfileView.dart';
import 'package:counter_app/view/view_account/SearchPeople.dart';
import 'package:counter_app/view_model/ManageAccountViewModel.dart';
import 'package:counter_app/view_model/ShoppingListItemViewModel.dart';
import 'package:counter_app/view_model/shopping_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'ShoppingListItemView.dart';
import 'view_account/LoginPage.dart';

class ShoppingListView extends StatefulWidget {
  @override
  _ShoppingListViewState createState() => _ShoppingListViewState();
}

class _ShoppingListViewState extends State<ShoppingListView> {
  ManageAccountViewModel manageAccountViewModel;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    NotificationHandler().initializeFcmNotification(context);
  }

  @override
  Widget build(BuildContext context) {
    final ShoppingListItemViewModel _shoppingListItemModel =
        Provider.of<ShoppingListItemViewModel>(context);
    manageAccountViewModel = Provider.of<ManageAccountViewModel>(context);
    manageAccountViewModel.isExistToken();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text('My List'),
        actions: <Widget>[
          Visibility(
            visible: manageAccountViewModel.isLogin,
            child: IconButton(
              icon: Icon(
                Icons.person,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MyProfileView()),
                );
              },
            ),
          )
        ],
      ),
      body: Consumer<ShoppingListViewModel>(
        builder: (BuildContext context, viewModel, Widget child) {
          if (viewModel.state == ShoppingListViewModelState.INITIAL) {
            viewModel.allList();
          }
          if (viewModel.state == ShoppingListViewModelState.LOADED) {
            var shoppingList = viewModel.shoppingList;
            if (shoppingList.isNotEmpty) {
              return ListView.separated(
                  reverse: true,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 20.0,
                      margin: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 3.0),
                      child: Container(
                        decoration: BoxDecoration(
                          color: Theme.of(context).cardColor,
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: ListTile(
                          contentPadding: EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 20.0),
                          leading: Container(
                              padding: EdgeInsets.only(right: 10.0),
                              decoration: BoxDecoration(
                                border: Border(
                                  right:
                                      BorderSide(width: 1.0, color: Colors.red),
                                ),
                              ),
                              child: IconButton(
                                  icon: Icon(
                                    Icons.share,
                                    color: Colors.black,
                                    size: 35.0,
                                  ),
                                  onPressed: () {
                                  viewModel.shoppingListName =
                                  _findShoppingListName(shoppingList, index);
                                    forwardingOperaitons();
                                  })),
                          title: Text(
                            _findShoppingListName(shoppingList, index),
                            style: Theme.of(context).textTheme.title,
                          ),
                          trailing: IconButton(
                              icon: Icon(
                                Icons.restore_from_trash,
                                color: Colors.black,
                                size: 40.0,
                              ),
                              onPressed: () => deleteActionButton(
                                  viewModel, shoppingList, index)),
                          onTap: () {
                            viewModel.shoppingListName =
                                _findShoppingListName(shoppingList, index);
                            var shoppingListId =
                                _findShoppingListId(shoppingList, index);
                            _shoppingListItemModel
                                .inquireItemList(shoppingListId);
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) {
                              return ShoppingListItemView(shoppingListId);
                            }));
                          },
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                  itemCount: shoppingList.length);
            } else {
              return Center(
                child: Text(
                  'Alışveriş listesi eklemek için + ya dokunun!',
                  style: Theme.of(context).textTheme.title,
                ),
              );
            }
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        tooltip: 'Add Shppping List',
        child: Icon(Icons.add),
        heroTag: "ShoppingListCreate",
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ShoppingListCreateView()),
          );
        },
      ),
    );
  }

  int _findShoppingListId(List<ShoppingList> shoppingList, int index) =>
      shoppingList[index].shoppingListId;

  String _findShoppingListName(List<ShoppingList> shoppingList, int index) =>
      shoppingList[index].name;

  Future<void> deleteActionButton(ShoppingListViewModel viewModel,
      List<ShoppingList> shoppingList, int index) async {
    bool yesNoFlag = await CustomAlertDiolog(
      title: OperationResultCodeEnum.WARNING.toShortString(),
      description: 'Are you sure you want to delete?',
      mainButtonText: 'YES',
      generalButtonText: 'NO',
    ).show(context);

    if (yesNoFlag != null && yesNoFlag) {
      viewModel.deleteShoppingList(_findShoppingListId(shoppingList, index));
    }
  }

  Future<void> forwardingOperaitons() async {
    if (!manageAccountViewModel.isLogin) {
      bool yesNoFlag = await CustomAlertDiolog(
        title: OperationResultCodeEnum.WARNING.toShortString(),
        description: 'You must be logged in to share!',
        mainButtonText: 'LOGIN',
        generalButtonText: 'BACK',
      ).show(context);
      if (yesNoFlag) {
        //Todo Daha sonra LoginPage sayfasındaki kurallara göre işlemler yapılacaktır.
        Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
      }
    } else {
      bool yesNoFlag = await CustomAlertDiolog(
        title: OperationResultCodeEnum.INFORMATION.toShortString(),
        description: 'You must be logged in to share!',
        mainButtonText: 'SEARCH PEOPLE',
        generalButtonText: 'BACK',
      ).show(context);
      if(yesNoFlag) {
        Navigator.push(context, MaterialPageRoute(
          builder: (context) => SearchPeople()
        ));
      }
    }
  }
}

// Dart dilindeki enum kullanımı JAVAYA göre bayağı bir berbat durumda
extension on OperationResultCodeEnum {
  String toShortString() {
    return this.toString().split('.').last;
  }
}
