import 'package:counter_app/model/OperationResult.dart';

import 'OperationResultCodeEnum.dart';

class CommonUtility {

  static bool isOperationResultSuccess(OperationResult operationResult){
    if(OperationResultCodeEnum.SUCCESS.toString().contains(operationResult.operationResultCode)){
      return true;
    }
    return false;
  }
}