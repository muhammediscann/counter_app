import 'package:counter_app/di.dart';
import 'package:counter_app/view/ShoppingListView.dart';
import 'package:counter_app/view_model/ManageAccountViewModel.dart';
import 'package:counter_app/view_model/ProductViewModel.dart';
import 'package:counter_app/view_model/ShoppingListItemViewModel.dart';
import 'package:counter_app/view_model/shopping_list_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'view_model/SearchPeopleViewModel.dart';

void main() {
  setup();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ShoppingListViewModel>(
          create: (_) => ShoppingListViewModel(),
        ),
        ChangeNotifierProvider<ProductViewModel>(
          create: (_) => ProductViewModel(),
        ),
        ChangeNotifierProvider<ShoppingListItemViewModel>(
          create: (_) => ShoppingListItemViewModel(),
        ),
        ChangeNotifierProvider<ManageAccountViewModel>(
          create: (_) => ManageAccountViewModel(),
        ),
        ChangeNotifierProvider<SearchPeopleViewModel>(
          create: (_) => SearchPeopleViewModel(),
        ),
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
            primarySwatch: Colors.blue,
            backgroundColor: Colors.white,
            textTheme: TextTheme(
              overline: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.normal,
                  color: Colors.blue),
              button: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.normal,
                  color: Colors.blue),
              title: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.normal,
                  color: Colors.black),
              body1: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.normal,
                  fontStyle: FontStyle.normal,
                  color: Colors.black),
              body2: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.normal,
                  fontStyle: FontStyle.normal,
                  color: Colors.white
              )
            ),
            iconTheme: IconThemeData(color: Colors.white, size: 30.0),
            appBarTheme: AppBarTheme(
                color: Color.fromRGBO(58, 66, 86, 1.0),
                brightness: Brightness.dark,
                actionsIconTheme:
                    IconThemeData(color: Colors.white, size: 40.0),
                textTheme: TextTheme(
                  title: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      fontStyle: FontStyle.normal),
                ),
                elevation: 10.0),
            cardColor: Color.fromRGBO(220, 220, 220, 1),
            focusColor: Colors.blue,
            accentColor: Color.fromRGBO(180, 180, 180, 1),
            accentTextTheme: TextTheme(
              title: TextStyle(
                  fontSize: 20.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.normal),
              button: TextStyle(
                  fontSize: 22.0,
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.normal),
            ),
            buttonTheme: ButtonThemeData(
              textTheme: ButtonTextTheme.primary,
            )),
        home: ShoppingListView(),
      ),
    );
  }
}
