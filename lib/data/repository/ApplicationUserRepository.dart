import 'package:counter_app/data/ApplicationUserClient.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/ApplicationUser.dart';
import 'package:counter_app/model/JwtToken.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:flutter/foundation.dart';

class ApplicationUserRepository {
  var _applicationUserClient = getIt<ApplicationUserClient>();

  Future<OperationResult> registerApplicationUser(
      ApplicationUser applicationUser) async {
    return await _applicationUserClient.registerUser(applicationUser);
  }

  Future<OperationResult> updateRegisterApplicationUser(
      ApplicationUser applicationUser) async {
    return await _applicationUserClient.updateRegisterUser(applicationUser);
  }

  Future<OperationResult> loginApplicationUser(
      ApplicationUser applicationUser) async {
    return await _applicationUserClient.loginUser(applicationUser);
  }

  Future<ApplicationUser> inquireApplicationUser(
      ApplicationUser applicationUser) async {
    return await _applicationUserClient.inquireApplicationUser(applicationUser);
  }

  Future<ApplicationUser> searchApplicationUser(
      String username) async {
    return await _applicationUserClient.searchApplicationUser(username);
  }
}
