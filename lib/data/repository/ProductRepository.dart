import 'package:counter_app/data/ProductClient.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/Product.dart';

class ProductRepository {

  ProductClient _productClient = getIt<ProductClient>();

  Future<List<Product>> inquirePopularProductList() async {
    return await _productClient.inquirePopularProductList();
  }
}