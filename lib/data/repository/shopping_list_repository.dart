import 'package:counter_app/model/shopping_list.dart';

import '../../di.dart';
import '../shopping_list_client.dart';

class ShoppingListRepository {
  ShoppingListClient shoppingListClient = getIt<ShoppingListClient>();

  Future<ShoppingList> addShoppingList(String shoppingListName) async {
    return await shoppingListClient.addShoppingList(shoppingListName);
  }

  Future<ShoppingList> deleteShoppingList(int shoppingListId) async {
    return await shoppingListClient.deleteShoppingList(shoppingListId);
  }

  Future<List<ShoppingList>> inquireAll() async {
    return await shoppingListClient.inquireAllShoppingList();
  }
}
