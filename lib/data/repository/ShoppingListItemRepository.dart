import 'package:counter_app/data/ShoppingListItemClient.dart';
import 'package:counter_app/di.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:counter_app/model/Product.dart';
import 'package:counter_app/model/ShoppingListItem.dart';

class ShoppingListItemRepository {
  ShoppingListItemClient _shoppingListItemClient =
      getIt<ShoppingListItemClient>();

  Future<List<ShoppingListItem>> addShoppingListItem(List<Product> productList, int shoppingListId) async {
    return await _shoppingListItemClient.addShoppingListItem(
        productList, shoppingListId);
  }

  Future<List<ShoppingListItem>> inquireShoppingListItemList(
      int shoppingListId) async {
    return await _shoppingListItemClient
        .inquireShoppingListItemList(shoppingListId);
  }

  Future<OperationResult> deleteShoppingListItemList(
      List<int> shoppingListItemIdList) async {
    return await _shoppingListItemClient
        .deleteShoppingListItemList(shoppingListItemIdList);
  }
}
