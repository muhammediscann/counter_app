import 'package:counter_app/model/FirebaseToken.dart';
import 'package:counter_app/model/OperationResult.dart';

import '../../di.dart';
import '../FirebaseTokenClient.dart';

class FirebaseTokenRepository {
  var _firebaseTokenClient = getIt<FirebaseTokenClient>();

  Future<OperationResult> createFirebaseToken(
      FirebaseToken firebaseToken) async {
    return await _firebaseTokenClient.createFirebaseToken(firebaseToken);
  }
}
