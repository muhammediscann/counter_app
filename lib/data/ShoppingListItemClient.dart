import 'dart:convert';

import 'package:counter_app/model/OperationResult.dart';
import 'package:counter_app/model/Product.dart';
import 'package:counter_app/model/ShoppingListItem.dart';
import 'package:counter_app/model/shopping_list.dart';
import 'package:http/http.dart' as http;

class ShoppingListItemClient {
  //static const baseUrl = "https://shopping-list-app-flu.herokuapp.com/item";
  static const baseUrl = "http://192.168.1.2:8080/item";
  final http.Client httpClient = http.Client();

  Future<List<ShoppingListItem>> addShoppingListItem(
      List<Product> willAddProductList, int shoppingListId) async {
    List<ShoppingListItem> willAddShoppingListItemList =
        _buildShoppingListItemList(willAddProductList, shoppingListId);
    String codi = jsonEncode({"shoppingListItemList" : jsonDecode(ShoppingListItem.shoppingListItemToJson(
        willAddShoppingListItemList))});
    var pathUrl = baseUrl + "/save";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: codi
    );
    if (response.statusCode != 200) {
      print("Error : ${response.statusCode}");
    }

    return ShoppingListItem.shoppingListItemFromJsonList(response.body);
  }

  List<ShoppingListItem> _buildShoppingListItemList(
      List<Product> willAddProductList, int shoppingListId) {
    List<ShoppingListItem> shoppingListItemList = List();
    willAddProductList.forEach((product) {
      ShoppingListItem shoppingListItem = ShoppingListItem();
      shoppingListItem.product = product;
      shoppingListItem.shoppingList = ShoppingList.withoutName(shoppingListId);
      shoppingListItemList.add(shoppingListItem);
    });
    return shoppingListItemList;
  }

  Future<List<ShoppingListItem>> inquireShoppingListItemList(
      int shoppingListId) async {
    var pathUrl = baseUrl + "/inquire/$shoppingListId";
    var response = await httpClient.get(pathUrl);

    if (response.statusCode != 200) {
      print("Error : ${response.statusCode}");
    }

    return ShoppingListItem.shoppingListItemFromJsonList(response.body);
  }

  Future<OperationResult> deleteShoppingListItemList(
      List<int> shoppingListItemIdList) async {
    var pathUrl = baseUrl + "/remove";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode({
          "shoppingListItemIdList": shoppingListItemIdList,
        }));

    if (response.statusCode != 200) {
      print("Error : ${response.statusCode}");
    }
    return OperationResult.operationResultFromJson(response.body);
  }
}
