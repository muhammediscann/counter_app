import 'dart:convert';

import 'package:counter_app/model/ApplicationUser.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class ApplicationUserClient {
  static const baseUrl = "http://192.168.1.2:8080/users";
  final http.Client httpClient = http.Client();

  Future<OperationResult> registerUser(ApplicationUser applicationUser) async{
    var pathUrl = baseUrl + "/register";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(
              {
                "username" : applicationUser.username,
                "email" : applicationUser.email,
                "password" : applicationUser.password
              }
            )
        );

    if(response.statusCode != 200){
      /*
      * TODO Daha sonra @ControllerAdvice ile hata yakalanıp OperationResultException şekilnde buraya dönülebilir.
      */
      return OperationResult(operationResultCode: "ERROR", description: response.body);
    }
    return OperationResult.operationResultFromJson(response.body);
  }

  Future<OperationResult> updateRegisterUser(ApplicationUser applicationUser) async{
    var pathUrl = baseUrl + "/register/update";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(
            {
              "applicationUserId" : applicationUser.applicationUserId,
              "username" : applicationUser.username,
              "email" : applicationUser.email,
              "password" : applicationUser.password
            }
        )
    );

    if(response.statusCode != 200){
      /*
      * TODO Daha sonra @ControllerAdvice ile hata yakalanıp OperationResultException şekilnde buraya dönülebilir.
      */
      return OperationResult(operationResultCode: "ERROR", description: response.body);
    }
    return OperationResult.operationResultFromJson(response.body);
  }

  Future<OperationResult> loginUser(ApplicationUser applicationUser) async{
    SharedPreferences _sharedPreferences = await SharedPreferences.getInstance();

    var pathUrl = "http://192.168.1.2:8080/login";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(
            {
              "username" : applicationUser.username,
              "password" : applicationUser.password
            }
        )
    );

    if(response.statusCode != 200){
      return OperationResult(operationResultCode: "ERROR", description: "Token didn't save!");
    }

    await _sharedPreferences.setString("jwt_token", response.headers['authorization']);
    return OperationResult(operationResultCode: "SUCCESS", description: "Token saved!");
  }

  Future<ApplicationUser> inquireApplicationUser(ApplicationUser applicationUser) async {
    var pathUrl = baseUrl + "/inquire/${applicationUser.username}";
    var response = await httpClient.get(pathUrl);

    if(response.statusCode != 200) {
      print("Error : ${response.statusCode}");
    }

    if(response.body != ""){
      return ApplicationUser.applicationUserFromJson(response.body);
    }

    return null;
  }

  Future<ApplicationUser> searchApplicationUser(String username) async {
    var pathUrl = baseUrl + "/search/$username";
    var response = await httpClient.get(pathUrl);

    if(response.statusCode == 200 && response.body != ""){
      return ApplicationUser.applicationUserFromJson(response.body);
    }

    return null;
  }
}
