import 'dart:convert';
import 'package:counter_app/model/FirebaseToken.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:http/http.dart' as http;

class FirebaseTokenClient{
  static const baseUrl = "http://192.168.1.2:8080/firebase";
  final http.Client httpClient = http.Client();

  Future<OperationResult> createFirebaseToken(FirebaseToken firebaseToken) async{
    var pathUrl = baseUrl + "/create";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode(
            {
              "token" : firebaseToken.token,
              "applicationUser" : {
                "applicationUserId" : firebaseToken.applicationUserId
              }
            }));

    if(response.statusCode != 200){
      return OperationResult(operationResultCode: "ERROR", description: "Something went wrong!");
    }

    return OperationResult.operationResultFromJson(response.body);
  }
}