import 'dart:convert';

import 'package:counter_app/model/Product.dart';
import 'package:http/http.dart' as http;

class ProductClient {
  static const baseUrl = "http://192.168.1.2:8080/product";
//  static const baseUrl = "https://shopping-list-app-flu.herokuapp.com/product";
  final http.Client httpClient = http.Client();

  Future<List<Product>> inquirePopularProductList() async {
    var pathUrl = baseUrl + "/inquire/popular";
    var response = await httpClient.get(pathUrl, headers: {'Content-Type': 'application/json; charset=utf-8'});

    if(response.statusCode != 200){
      print("Error : ${response.statusCode}");
    }

    return Product.productFromJson(response.body);
  }
}