import 'dart:convert';

import 'package:counter_app/di.dart';
import 'package:counter_app/model/ApplicationUser.dart';
import 'package:counter_app/model/OperationResult.dart';
import 'package:counter_app/model/shopping_list.dart';
import 'package:counter_app/utility/CommonUtility.dart';
import 'package:http/http.dart' as http;
import 'package:random_string/random_string.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'repository/ApplicationUserRepository.dart';

class ShoppingListClient {
  //static const baseUrl = "https://shopping-list-app-flu.herokuapp.com/shopping_list";
  static const baseUrl = "http://192.168.1.2:8080/shopping_list";
  final http.Client httpClient = http.Client();
  final _applicationUserRepository = getIt<ApplicationUserRepository>();

  Future<ShoppingList> addShoppingList(String shoppingListName) async {
    SharedPreferences _sharedPreferences = await SharedPreferences.getInstance();
    String username = _sharedPreferences.getString("username");
    ApplicationUser applicationUser = await _applicationUserRepository
        .inquireApplicationUser(ApplicationUser.withoutPass(username));
    if(applicationUser != null) {
      var pathUrl = baseUrl + "/save";
      var response = await httpClient.post(pathUrl,
          headers: <String, String>{
            'Content-Type': 'application/json',
          },
          body: jsonEncode(
              {
                "name" : shoppingListName,
                "applicationUser" : {
                  "applicationUserId" : applicationUser.applicationUserId
                }
              }));

      if(response.statusCode != 200){
        return null;
      }

      return ShoppingList.shoppingListFromJson(response.body);
    }

    return null;
  }

  Future<ShoppingList> deleteShoppingList(int shoppingListId) async {
    var pathUrl = baseUrl + "/remove";
    var response = await httpClient.post(pathUrl,
        headers: <String, String>{
          'Content-Type': 'application/json',
        },
        body: jsonEncode({"shoppingListId" : shoppingListId}));

    if(response.statusCode != 200){
      print("Error : ${response.statusCode}");
    }
    return ShoppingList.shoppingListFromJson(response.body);
  }

  Future<List<ShoppingList>> inquireAllShoppingList() async {
    SharedPreferences _sharedPreferences = await SharedPreferences.getInstance();
    String username = _sharedPreferences.getString("username");
    if(username == null) {
      username = randomAlphaNumeric(15);
      final OperationResult _operationResult = await _applicationUserRepository
          .registerApplicationUser(ApplicationUser.withoutPass(username));
      if(CommonUtility.isOperationResultSuccess(_operationResult)) {
        await _sharedPreferences.setString("username", username);
      }
    }
    var pathUrl = baseUrl + "/inquireBy/$username";
    var response = await httpClient.get(pathUrl);

    if(response.statusCode != 200) {
      print("Error : ${response.statusCode}");
    }
    return ShoppingList.shoppingListFromJsonList(response.body);
  }
}